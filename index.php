<?php
require 'application/lib/Dev.php';
use application\core\Router;
use application\lib\Db;

auto_routes();
session_start();

$router = new Router;
$router->run();
