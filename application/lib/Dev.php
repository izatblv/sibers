<?php
ini_set('display_errors', 1);
error_reporting('E_ALL');

function test()
{
    static $index;
    $index = +$index + 1;
    p("TEST " . $index);
}

function p($text)
{
    echo "$text";
    echo '<br>';
}

function p_b($text)
{
    echo "<b>$text</b>";
    echo '<br>';
}

function d($arr)
{
    debug($arr);
}

function d2($arr)
{
    debug2($arr);
}

function debug($arr)
{
    echo '<pre>' . print_r($arr, true) . '</pre>';
}

function debug2($str)
{
    echo '<pre>';
    var_dump($str);
    echo '</pre>';
}

function auto_routes()
{
    spl_autoload_register(function ($class) {
        $path = str_replace('\\', '/', $class . '.php');
//    echo $path;
        if (file_exists($path)) {
            require $path;
        }
    });
}