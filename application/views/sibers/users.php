<h1>Пользователи</h1>
<p>Последние 5 зарегестрированных пользователей</p>
<table>
    <tr>
        <?php
        $table = ["Имя", "Фамилия", "Пол", "Дата рождения"];
        foreach ($table as $el) { ?>
            <td><b><?= $el ?></b></td>
        <?php } ?>
    </tr>
    <?php foreach ($vars as $el) { ?>
        <tr>
            <td><?= $el['name'] ?></td>
            <td><?= $el['surname'] ?></td>
            <td><?= $el['gender'] ?></td>
            <td><?= $el['dob'] ?></td>
        </tr>
    <?php } ?>
</table>