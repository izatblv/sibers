<h1>Добро пожаловать в тестовое задание Sibers</h1>
<p>
    Как и было указано в задании в репозитории по <a
            href="https://bitbucket.org/izatblv/sibers/src/master/application/assets/PHP-test-1.pdf" target="_blank">
        ссылке</a><br>
    данный сайт написан на языке PHP без готовых фреймворков таких как Angular, Yii, Symfony, Laravel и т.п.
</p>
<p>Данный сайт был написан с нуля, используя модель MVC.</p>

<p>База данных располагается в репозитории по <a href="https://bitbucket.org/izatblv/sibers/src/master/application/db/db.sql"
    target="_blank">ссылке</a><br>

    <h1>Репозиторий</h1>
<a href="https://bitbucket.org/izatblv/sibers/src/master/" target="_blank">ссылка</a>
</p>

<img src="application/assets/img/pic.jpg" style="width: 100%">


