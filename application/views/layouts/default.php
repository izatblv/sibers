<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?= $title ?>
    </title>
    <link rel="stylesheet" href="application/assets/css/csstool.css">
</head>
<body>
<div class="main">
<div style="height: 22px">
    <?php
    $menu_elements=[
        "<div class=\"menu3\"><img src=\"application/assets/img/logo.png\" style=\"height: 20px\"></div>",
        "<a href='/sibers'><div class=\"menu2\">Главная</div></a>",
        "<a href='/users'><div class=\"menu2\">Пользователи</div></a>",
        "<a href='/admin'><div class=\"menu2\">Администратор</div></a>",
    ]
    ?>
    <?php foreach ($menu_elements as $val):?>
        <div class="plitki menu"><?=$val?></div>
    <?php endforeach; ?>
</div>
<div><?= $content ?></div>
    </div>
</body>
</html>