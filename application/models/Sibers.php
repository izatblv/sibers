<?php

namespace application\models;

use application\core\Model;

class Sibers extends Model
{
    public function users()
    {
        $users = $this->db->row('SELECT * FROM users ORDER BY `id` DESC LIMIT 0,5');
        return $users;
    }

    public function users_create()
    {
        $login_list = $this->db->row('SELECT login FROM users');
        return $login_list;
    }

    public function admin($by, $sort)
    {
        if ($sort == 1) {
            $sort = "ASC";
        }
        if ($sort == 2) {
            $sort = "DESC";
        }
        $users = $this->db->row('SELECT * FROM users ORDER BY `' . $by . '` ' . $sort . ' LIMIT 0,100');
        return $users;
    }

    public function user_id()
    {
        $users = $this->db->row('SELECT * FROM users WHERE id=' . $_GET['id']);
        return $users;
    }

    public function update($k)
    {
        $this->db->row('UPDATE `users` SET 
`login`=\'' . $k['login'] . '\',
`password`=\'' . $k['password'] . '\',
`name`=\'' . $k['name'] . '\',
`surname`=\'' . $k['surname'] . '\',
`gender`=\'' . $k['gender'] . '\',
`dob`=\'' . $k['dob'] . '\'
WHERE `users`.`id` =' . $k['id']);
    }

    public function delete()
    {
        $this->db->row('DELETE FROM `users` WHERE `users`.`id` =' . $_GET['id']);
    }

    public function create()
    {
        $user = $this->db->row('SELECT * FROM users ORDER BY id DESC LIMIT 1;');
        return $user;
    }

    public function create_do($k)
    {
        $this->db->row('INSERT INTO `users` 
(`id`, `login`, `password`, `name`, `surname`, `gender`, `dob`) VALUES 
(NULL, \'' . $k['login'] . '\', \'' . $k['password'] . '\', \'' . $k['name'] . '\', \'' . $k['surname'] . '\', \'' . $k['gender'] . '\', \'' . $k['dob'] . '\');
');
    }


}
