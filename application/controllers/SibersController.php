<?php

namespace application\controllers;

use application\core\Controller;

class SibersController extends Controller
{
    public function indexAction()
    {
        $this->view->render("Hello Sibers", $vars);
    }

    public function usersAction()
    {
        $vars = $this->model->users();
        $this->view->render("Users", $vars);
    }

    public function adminAction()
    {
        if ($_SESSION['autorization'] != 1) {
            header("Location: login");
        }
        if ($_GET) {
        } else {
            $this->go_back_admin();
        }
        $vars = $this->model->admin($_GET['by'], $_GET['sort']);
        $this->view->render("Admin", $vars);
    }

    public function user_viewAction()
    {
        $vars = $this->model->user_id($_GET['id']);
        $this->view->render("Admin", $vars);
    }

    public function user_editAction()
    {
        if ($_SESSION['autorization'] != 1) {
            header("Location: login");
        }
        $vars = $this->model->user_id();
        $this->view->render("Edit user", $vars);
    }

    public function user_updateAction()
    {
        if ($_SESSION['autorization'] != 1) {
            header("Location: login");
        }
        $this->model->update($_POST);
        $this->go_back_admin();
    }

    public function user_deleteAction()
    {
        if ($_SESSION['autorization'] != 1) {
            header("Location: login");
        }
        $this->model->delete();
        $this->go_back_admin();
    }

    public function user_createAction()
    {
        if ($_SESSION['autorization'] != 1) {
            header("Location: login");
        }
        $vars = $this->model->create();
        $vars['users']=$this->model->users_create();
        $this->view->render("New user", $vars);
        unset($_SESSION['errors']);
    }

    public function user_create_doAction()
    {
        $error=false;
        if (isset($_POST['send'])) {
            if(!preg_match('/^[0-9\p{L}\s]+$/', $_POST['login'])){$error['login']='Неправильный логин';}
            $login_list=$this->model->users_create();
            foreach ($login_list as $k){
                if ($_POST['login']==$k['login']){$error['login']='Логин занят';}
            }
            if(!preg_match('/^[0-9\p{L}\s]+$/', $_POST['password'])) $error['password']="Неправильный пароль";
            if(!preg_match('/^([а-яА-ЯЁёa-zA-Z_]+)$/u', $_POST['name'])) $error['name']="Неправильное имя";
            if(!preg_match('/^([а-яА-ЯЁёa-zA-Z_]+)$/u', $_POST['surname'])) $error['surname']="Неправильное фамилие";
            if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST['dob'])) $error['dob']="Неправильная дата";
        }
        if(!$error){
            unset($_SESSION['post']);
            $this->model->create_do($_POST);
            $this->go_back_admin();

        }else{

                $_SESSION['post']=$_POST;
                $_SESSION['errors']=$error;
                header("Location: create");
        }
    }

    public function go_back_admin()
    {
        header("Location: admin?&by=id&sort=2&page=0");
    }

    public function admin_loginAction()
    {
        $this->view->render("admin_login", $vars);
    }

    public function autorizationAction()
    {
        if ($_POST['login'] == 'admin' && $_POST['password'] == 'password') {
            $_SESSION['autorization'] = 1;
            $this->go_back_admin();
        } else {
            $_SESSION['log_resul'] = "Вы ввели неправильный логин или пароль";
            header("Location: login");
        }
    }

    public function generateAction()
    {
        $keys = [
            'login', 'password', 'name', 'surname', 'gender', 'dob'
        ];
        $mnamef = [
            'Иван', 'Гриша', 'Петя', 'Филип', 'Артур', 'Юра', 'Коля', 'Сергей'
        ];
        $msurname = [
            'Соколов', 'Новиков', 'Маликов', 'Киркоров', 'Губин', 'Федеров', 'Егоров', 'Павлов'
        ];
        function testf($el)
        {
            return $el[rand(0, count($el) - 1)];
        }

        function testf2($el)
        {
            return $el[rand(0, count($el) - 1)];
        }

        $keys_var[$keys[0]] = "login" . rand(100, 999);
        $keys_var[$keys[1]] = "password" . rand(100, 999);
        $keys_var[$keys[2]] = testf($mnamef);
        $keys_var[$keys[3]] = testf2($msurname);
        $keys_var[$keys[4]] = "M";
        $keys_var[$keys[5]] = rand(1980, 2000) . "-" . rand(1, 12) . "-" . rand(1, 20);
        $this->model->create_do($keys_var);
        $this->go_back_admin();
    }
}