<?php
return [
    '' => [
        'controller' => 'sibers',
        'action' => 'index',
    ],
    'sibers' => [
        'controller' => 'sibers',
        'action' => 'index',
    ],
    'users' => [
        'controller' => 'sibers',
        'action' => 'users',
    ],
    'admin' => [
        'controller' => 'sibers',
        'action' => 'admin',
    ],
    'view' => [
        'controller' => 'sibers',
        'action' => 'user_view',
    ],
    'edit' => [
        'controller' => 'sibers',
        'action' => 'user_edit',
    ],
    'update' => [
        'controller' => 'sibers',
        'action' => 'user_update',
    ],
    'delete' => [
        'controller' => 'sibers',
        'action' => 'user_delete',
    ],
    'create' => [
        'controller' => 'sibers',
        'action' => 'user_create',
    ],
    'create_do' => [
        'controller' => 'sibers',
        'action' => 'user_create_do',
    ],
    'login' => [
        'controller' => 'sibers',
        'action' => 'admin_login',
    ],
    'autorization' => [
        'controller' => 'sibers',
        'action' => 'autorization',
    ],
    'generate' => [
        'controller' => 'sibers',
        'action' => 'generate',
    ],
];
