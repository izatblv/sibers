-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 18 2018 г., 17:13
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Sibers`
--
CREATE DATABASE IF NOT EXISTS `Sibers` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `Sibers`;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(11) NOT NULL,
  `password` varchar(33) NOT NULL,
  `name` varchar(11) NOT NULL,
  `surname` varchar(11) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `dob` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `surname`, `gender`, `dob`) VALUES
(155, 'login780', 'password646', 'Филип', 'Павлов', 'M', '1996-07-15'),
(156, 'login384', 'password861', 'Юра', 'Павлов', 'M', '1987-10-15'),
(157, 'login774', 'password908', 'Петя', 'Егоров', 'M', '1989-05-17'),
(158, 'login256', 'password583', 'Юра', 'Киркоров', 'M', '1989-09-11'),
(159, 'login608', 'password478', 'Коля', 'Киркоров', 'M', '1995-07-10'),
(160, 'login201', 'password180', 'Коля', 'Павлов', 'M', '1998-05-04'),
(161, 'login298', 'password668', 'Артур', 'Киркоров', 'M', '1997-04-10'),
(162, 'login246', 'password680', 'Артур', 'Маликов', 'M', '1997-09-16'),
(163, 'login237', 'password658', 'Гриша', 'Соколов', 'M', '1992-05-05'),
(164, 'login149', 'password731', 'Филип', 'Федеров', 'M', '1985-10-13'),
(165, 'login732', 'password416', 'Коля', 'Соколов', 'M', '1995-03-11'),
(166, 'login802', 'password350', 'Коля', 'Павлов', 'M', '1998-12-20'),
(167, 'login625', 'password774', 'Юра', 'Павлов', 'M', '1999-09-06');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
